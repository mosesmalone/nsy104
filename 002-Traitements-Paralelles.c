/***************************************************
    EX0 2 | TRAITEMENTS PARALLELES
****************************************************
    > Construit une équipe de threads
    > Distribue les éléments aux threads
    > Informe quel thread traite quel élément
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main (int argc, char const *argv[])
{
    int n;

    #pragma omp parallel num_threads(4)
    {
        for(n=0;n<8;n++){
            printf("Element %d traité par le thread %d \n",n,omp_get_thread_num());
        }
    }
    return EXIT_SUCCESS;
}