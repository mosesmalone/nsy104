#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, num_step;
    double step, sum, x, pi;

    num_step = 100000;
    step = 1. / num_step;

    for (i = 0; i < num_step; i++)
    {
        x = (i + 0.5) * step;
        sum += 4. / (1. + x * x);
    }

    pi = step * sum;
    printf("Pi = %lf\n", pi);
}