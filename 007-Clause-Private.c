#include <stdio.h>
#include <omp.h>

int main()
{
    float a;
    a = 92000.0;

    #pragma omp parallel private(a)
    {
        printf("a vaut : %f\n",a);
        a = a + 290.0;
        printf("a vaut : %f\n",a);
    }

    return 0;
}