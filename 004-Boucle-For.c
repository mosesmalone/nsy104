#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main (int argc, char const *argv[]){
    int n;
    
    #pragma omp parallel num_threads(4) // Crée une équipe de 4 threads
    #pragma omp for                     // Ditribue les itérations de la boucle suivante à chaque thread de l'équipe précédemment créée
    for (n=0;n<8;n++) {
        printf("Element %d traité par le thread %d \n",n,omp_get_thread_num());
    }

    return EXIT_SUCCESS;
}