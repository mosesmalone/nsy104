
#include <omp.h>
#include <stdio.h>

static long num_steps = 1000000000.0;
long double step;
#define NUM_THREADS 5

int main()
{
    int i;
    long double pi, sum, x, step;

    sum = 0.0;
    pi = 0.0;
    x = 0.0;

    step = 1.0 / num_steps;

    omp_set_num_threads(NUM_THREADS);

    #pragma omp parallel for reduction(+:sum) private(i, x)
    for (i = 0; i < num_steps; i++)
    {
        x = (i + 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }

    pi = step * sum;

    printf("pi = %1.40Lg\n", pi);
    return 0;
}
