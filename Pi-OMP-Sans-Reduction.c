#include <stdio.h>
#include <stdlib.h>

#define NUM_THREADS 2
static long num_step = 100000;

int main()
{
    int i;
    double step, n_threads, sum[NUM_THREADS], pi;

    step = 1. / (double) num_step;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel
    {
        int id, i, n_thread;
        double x;
        
        id = omp_get_thread_num();
        n_thread = omp_get_num_threads();

        if (id == 0) n_threads = n_thread;

        for (i=id, sum[id]=0.0;i< num_step; i=i+n_thread) {
            x = (i+0.5)*step;
            sum[id] += 4.0/(1.0+x*x);
        }
    }

    for(i=0, pi=0.0;i<n_threads;i++)pi += sum[i] * step;

    printf("Pi = %lf\n", pi);
    return 0;
}