#include <stdio.h>
#include <sys/time.h>

#define SIZE 10000

int main () {
    double a[ SIZE ], b[ SIZE ], c[ SIZE ];
    int i;

    for(i=0; i<SIZE ; i++)
    {
        a[i] = b[i] = i;
        c[i]=0;
    }

    for (i=0; i<SIZE ; i++)
    {
        c[i] = a[i] + b[i ];
        printf("==> %i\n", i);
    }

    for (i=0; i<SIZE ; i++)
    {
        printf("==> [%i ] = %g\n" , i , c[i]);
    }
    return 0;
}