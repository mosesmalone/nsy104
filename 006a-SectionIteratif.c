#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define LIMIT 1000000

int main()
{
    int a, b, c, i;

    a = 1;
    b = 2;
    c = 0;

    for (i = 0; i < LIMIT; i++)
    {
        c += a + b;
        printf("Itération n°%d => c = %d\n", i, c);
    }
    return 0;
}