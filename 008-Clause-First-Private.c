
#include <stdio.h>
#include <omp.h>

int main()
{
    float a;
    a = 92000.00;

    #pragma omp parallel default(none) firstprivate(a)
    {
        printf("a vaut : %f\n",a);
        a = a + 290.00;
        printf("a vaut : %f\n",a);
    }

    printf("Hors region, a vaut : %f\n", a);
    return 0;
}