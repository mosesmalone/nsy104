/***************************************************
    EX0 1 | CLAUSE IF
****************************************************
    Construit une équipe de threads
    Si l'expression scalaire PARALLEL != 0
    Sinon, exécute le code de manière séquentielle
****************************************************/

#include <stdio.h>

#define PARALLEL 1  // 0 => séquentiel | 1 => parallèle

int main()
{
    // Régions parallèles
    #pragma omp parallel if (PARALLEL)
    printf("Hello, openMP\n");

    return 0;
}