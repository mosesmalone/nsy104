# DOCUMENTATION OPENMP
> https://www.openmp.org/wp-content/uploads/OpenMP-API-Specification-5.0.pdf  
> http://jakascorner.com/blog/2016/06/omp-for-scheduling.html  
  
# 001 - Clause If
Construit une équipe de threads en fonction de **PARALLEL** :  
* Si l'expression scalaire PARALLEL != 0  
* Sinon, exécute le code de manière séquentielle  
  
## Compilation
**$** gcc -Wall -fopenmp -o 001-Clause-If 001-Clause-If.c  
  
## Exécution
**$** ./001-Clause-If  

## Résultat
Hello, openMP  
Hello, openMP  
Hello, openMP  
Hello, openMP  

# 002 - Traitements parallèles
* Construit une équipe de **4 threads**  
> **__num_threads(4)__**  
* Distribue les éléments aux threads  
* Informe quel thread traite quel élément  
  
## Compilation
**$** gcc -Wall -fopenmp -o 002-Traitements-Paralleles 002-Traitements-Paralleles.c  
  
## Exécution
**$** ./002-Traitements-Paralleles  

## Résultat
Element 0 traité par le thread 0  
Element 1 traité par le thread 0  
Element 2 traité par le thread 0  
Element 3 traité par le thread 0  
Element 4 traité par le thread 0  
Element 5 traité par le thread 0  
Element 6 traité par le thread 0  
Element 7 traité par le thread 0  
Element 0 traité par le thread 1  
Element 0 traité par le thread 2  
Element 1 traité par le thread 2  
Element 2 traité par le thread 2  
Element 3 traité par le thread 2  
Element 4 traité par le thread 2  
Element 5 traité par le thread 2  
Element 6 traité par le thread 2  
Element 7 traité par le thread 2  
Element 0 traité par le thread 3  

# 003 - Get Num Threads
Pour chaque équipe de threads créée, récupère le nombre de thread.
  
## Exécution
**$** ./003-Get-Num-Threads  
  
## Résultat
thread 0 --> A = 0.000000  
thread 3 --> A = 3.000000  
thread 2 --> A = 2.000000  
thread 1 --> A = 1.000000  
fin  
  
# 004 - Pragma Omp For
* Crée une équipe de 4 threads
* Ditribue les itérations de la boucle suivante à chaque thread de l'équipe précédemment créée
  
## Exécution
**$** ./004-Boucle-For  
  
# Résultat
Element 0 traité par le thread 0  
Element 1 traité par le thread 0  
Element 4 traité par le thread 2  
Element 5 traité par le thread 2  
Element 2 traité par le thread 1  
Element 3 traité par le thread 1  
Element 6 traité par le thread 3  
Element 7 traité par le thread 3  
  
# 005 - Clause Schedule
## a) Itératif
Calcule Ci de façon itérative et affiche les résultats  
  
## Exécution et calcul de temps
**$** time ./005a-Clause-Schedule-Iteratif  

## b) Schedule
Crée une équipe de threads, organise un planning de travail pour ces derniers et calcule Ci de façon itérative et affiche les résultats  
  
## Exécution et calcul de temps
**$** time ./005b-Clause-Schedule-Final  
  
# 006 - Clause Section [ /!\ Non fonctionnel]
## a) Itératif
Calcule Ci de façon itérative et affiche les résultats  
  
## Exécution
**$** time ./006a-Clause-Section-Iteratif  
  
## Résultat
real    0m30,903s
user    0m0,825s
sys     0m2,586s

## b) Avec des threads par section
Crée des sections et calcule Ci et affiche les résultats  
  
## Exécution et calcul de temps
**$** time ./006b-Clause-Section-Final  
  
# 007 - Clause Private
Limite la visibilité d'une variable donnée en paramètre et modifie sa valeur une fois.  
La variable a est envoyé à chaque thread de l'équipe créée avec le caractère __privé__. 
Elle entre dans chacun d'eux comme __non initialisée__ et vaut 0 (fonctionnement de OpenMP).  
  
## Résultat
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
a vaut : 0.000000  
a vaut : 290.000000  
  
# 008 - Clause First Private
Avec la clause FIRSTPRIVATE, il est possible de forcer l'initialisation de cette variable privée à la dernière valeur qu'elle avait avant l'entrée dans la région parallèle.  
  
# 009 - Region parallèle étendue
Définie une région parallèle étendue qui n'engobe que l'appel de la fonction __sub()__. Cela permet impacte les variables locales de la méthode __sub()__ qui sont implicitement privées pour chaque tâche de l'équipe.  
  
# 010 - Clause Single
Crée une équipe de thread et définie une portion de code qui sera uniquement exécutée par le 1e thread qui agira.  
Si la clause **nowait** est indiquée après la clause **single**, les autres threads agiront comme OpenMP en a décidé. Sinon, ils devront attendre la fin du traitement du premier.  
  
