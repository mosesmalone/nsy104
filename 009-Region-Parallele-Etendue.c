#include <stdio.h>
#include <omp.h>

void sub(void)
{
    int i; int n; int sum;
    sum=0;

    for(i=0; i<1000; i++)
    {
        for(n=0; n<1000; n++)
        {
            sum=sum+1;
        }
    }
    printf("sum = %d\n", sum);
}

int main()
{
    void sub(void);

    #pragma omp parallel
    {
        sub();
    }
    return 0;
}