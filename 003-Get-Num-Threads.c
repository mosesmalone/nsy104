#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void fonc(int, double *);

int main (int argc, char const *argv[]){
    double A[1000];

    #pragma omp parallel num_threads(4)
    {
        int id = omp_get_thread_num();
        A[0]=id;
        fonc(id, A);
    }

    printf("fin\n");
    return EXIT_SUCCESS;
}

void fonc(int id, double *A)
{
    printf("thread %i --> A = %f\n", id, A[0]);
}